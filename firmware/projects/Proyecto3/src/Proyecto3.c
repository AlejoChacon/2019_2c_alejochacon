/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

#include "../inc/Proyecto3.h"
#include "led.h"
#include "systemclock.h"
#include "bool.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "uart.h"
#include "switch.h"
#include "timer.h"
/*==================[macros and definitions]=================================*/

bool Unidad = true;
uint16_t Distancia=0;

/*==================[internal functions declaration]=========================*/

/*
 * @fn ObteneDistancia()
 * @brief Funcion que obtiene la distancia calculada por el sensor ultrasónico HC-SR04 y la envía por protocolo RS232 al modulo
 * bluetooth HC-06
 * @return void
 */

void ObtenerDistancia(){

	if(Unidad==true){
		Distancia=HcSr04ReadDistanceCentimeters();
	}
	else{
		Distancia=HcSr04ReadDistanceInches();
	}

	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(Distancia, 10));

	if(Unidad==true){
		UartSendString(SERIAL_PORT_P2_CONNECTOR," cm \r\n");
	}
	else{
		UartSendString(SERIAL_PORT_P2_CONNECTOR," in \r\n");
	}

}

/*
 * @fn Useless()
 * @brief Funcion vacia para parametro *pSerial del inicializador de la UART
 * @return void
 */

void Useless(){

}

/*
 * @fn ElegirPulgadas()
 * @brief Funcion para alternar la unidad de medición, enciende un led si se está trabajando en pulgadas
 * @return void
 */

void ElegirPulgadas(){
	if(Unidad==true){
		Unidad=false;
		LedOn(LED_RGB_B);
	}
	else{
		Unidad=true;
		LedOff(LED_RGB_B);
	}
}


int main(void){

	SystemClockInit();
	HcSr04Init(T_FIL2, T_FIL3);
	LedsInit();

	timer_config TimerInitializer;
	TimerInitializer.timer=TIMER_B;
	TimerInitializer.period=1000;
	TimerInitializer.pFunc=ObtenerDistancia;
	TimerInit(&TimerInitializer);

	serial_config UARTInitializer;
	UARTInitializer.port=SERIAL_PORT_P2_CONNECTOR;
	UARTInitializer.baud_rate=9600;
	UARTInitializer.pSerial=Useless;
	UartInit(&UARTInitializer);

	SwitchesInit();
	SwitchActivInt(SWITCH_1, ElegirPulgadas);

	TimerStart(TIMER_B);
	while(1){

	}

	return 0;
}

/*==================[end of file]============================================*/

