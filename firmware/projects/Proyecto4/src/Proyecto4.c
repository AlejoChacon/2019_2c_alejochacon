/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto4.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "analog_io.h"
#include "timer.h"
#include "gpio.h"
#include "uart.h"
#include "bool.h"
#include "NeoPixel.h"
#include "fpu_init.h"

//#include "../../dsp/inc/arm_const_structs.h"
//#include "../../dsp/inc/arm_math.h"


#include "arm_math.h"
#include "arm_const_structs.h"


/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000
#define ARM_MATH_CM4
#define __FPU_PRESENT 1


#define IFFT_FLAG 0
#define DO_BIT_REVERSE 1

#define WINDOW_WIDTH 128

/*==================[internal data definition]===============================*/

uint8_t octava;
uint8_t nota;
float32_t frecuencia;
uint16_t VectorAD[2*WINDOW_WIDTH];
uint16_t Analog_single_data;
uint8_t Contador=0;
bool new_window = false;


/*==================[internal functions declaration]=========================*/

/*
 * @fn ContadorDeMuestras
 * @brief Llena VectorAD con los 100 valores analogicos convertidos
 * @return null
 */
void ContadorDeMuestras(){

	if(Contador<WINDOW_WIDTH){
		AnalogStartConvertion();
		VectorAD[Contador*2]=Analog_single_data*(3.3/1024);
		VectorAD[Contador*2+1]=0;
		Contador++;
	}
	else{
		TimerStop(TIMER_C);
		Contador=0;
		new_window = true;
	}
}

void Leer(){
	AnalogInputRead(CH1, &Analog_single_data);
}



/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	uint8_t i, i_max;
	float32_t fft[WINDOW_WIDTH], max = 0;

	analog_input_config ADC_Inicializar;
	ADC_Inicializar.input=CH1;
	ADC_Inicializar.mode=AINPUTS_SINGLE_READ;
	ADC_Inicializar.pAnalogInput=Leer;


	timer_config Inicializar_Timer;
	Inicializar_Timer.timer=TIMER_C;
	Inicializar_Timer.period=5;
	Inicializar_Timer.pFunc=ContadorDeMuestras;

	SystemClockInit();
	fpuInit();
	NeoPixelInit(GPIO1, 12);
	AnalogInputInit(&ADC_Inicializar);
	TimerInit(&Inicializar_Timer);


    while(1){
    	if(new_window){
    		TimerStart(TIMER_C);
    		/* Procesamiento para realizar la FFT */
    		arm_cfft_f32(&arm_cfft_sR_f32_len128, VectorAD, IFFT_FLAG, DO_BIT_REVERSE);
    		arm_cmplx_mag_f32(VectorAD, fft, WINDOW_WIDTH);
    		new_window = false;

    		/* Obtencion del valor maximo del espectro de frecuencias */
    		for(i=2 ; i<WINDOW_WIDTH ; i++){
    			if(fft[i]>max){
    		 	max = fft[i];
    		  	i_max = i;
    		    	}
    			}
    		frecuencia=fft[i_max];
    		max = 0;
    	}


	}
    
	return 0;
}

/*==================[end of file]============================================*/

