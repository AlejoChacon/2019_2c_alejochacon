/*
 * Tcrt5000.h
 *
 *  Created on: 6 sep. 2019
 *      Author: Alumno
 */

#ifndef TCRT5000_H_
#define TCRT5000_H_
#include "gpio.h"

/** @fn Tcrt5000Init(gpio_t dout)
 * @brief Funcion para inicializar sensor infrarrojo Tcrt5000
 * Setea la direccion del puerto GPIO para el pin de salida del sensor Tcrt5000
 * El puerto en uso queda guardado en variable gpio_t MyPin
 * @param[in] dout direccion del puerto GPIO a utilizar
 * returns booleano true si el puerto se inicializo correctamente
 */

bool Tcrt5000Init(gpio_t dout);

/* @fn Tcrt5000State(void)
 * @brief Funcion para obtener el estado actual del puerto de salida de Tcrt5000
 * returns booleano false cuando se detecta un obstaculo
 */

bool Tcrt5000State(void);

/*@fn Tcrt5000Deinit()
 * @brief Funcion para desinicializar puerto GPIO
 */

bool Tcrt5000Deinit();



#endif /* TCRT5000_H_ */
