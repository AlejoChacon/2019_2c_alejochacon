/*
 * DisplayITS_E0803.h
 *
 *  Created on: 6/09/2019
 *  Author: Alejandro Fidalgo Badell
 */

#ifndef DisplayITS_E0803_H
#define DisplayITS_E0803_H

/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"


/*==================[external functions declaration]=========================*/

/** @brief Initialization function of EDU-CIAA ITSE0803
 *
 * Set direction and initial state of lcd ports
 *
 * @param[in] pins
 *
 * @return TRUE if no error
 */
bool ITSE0803Init(gpio_t *pins);

/* @brief function to set value
 *
 * @param[in] valor
 *
 * @return TRUE if no error
 */

void ITSE0803DisplayValue(uint16_t valor);

/** @brief function to read the actual value of the display
 *
 * @param[in] No parameter
 *
 * @return actual value of display
 */
uint16_t ITSE0803ReadValue(void);
/** @brief Deinitialization function of EDU-CIAA ITSE0803
 *
 * @param[in] pins
 *
 * @return TRUE if no error
 */
bool ITSE0803Deinit(gpio_t *pins);

void DecimalToBcd(uint8_t digit);

/**@fn ITSE0803OFF()
 * @brief Turns off ITS-E0803 's screen
 *
 */
void ITSE0803OFF();

#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_DISPLAYITS_E0803_H_ */
