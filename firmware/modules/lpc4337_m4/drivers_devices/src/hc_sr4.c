/* @brief  EDU-CIAA NXP GPIO driver
 * @author Luna Paez Brenda
 *
 * This driver provide functions to generate delays using Timer0
 * of LPC4337
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/11/2018 | Document creation		                         |
 *
 */

#include "hc_sr4.h"
#include "chip.h"
#include "gpio.h"
#include "delay.h"


/*****************************************************************************
 * Private macros/types/enumerations/variables definitions
 ****************************************************************************/



/*****************************************************************************
 * Public types/enumerations/variables declarations
 ****************************************************************************/

/*****************************************************************************
 * Private functions definitions
 ****************************************************************************/

/*****************************************************************************
 * Private functions declarations
 ****************************************************************************/

/*****************************************************************************
 * Public functions declarations
 ****************************************************************************/

gpio_t ECHO, TRIGGER;
int16_t Distance_Centimeters, Distance_Inches;

bool HcSr04Init(gpio_t echo, gpio_t trigger)//esta función inicializa, dice echo es de entrada, y trigger de salida
	{
	ECHO=echo;
	TRIGGER=trigger;
	GPIOInit(ECHO,GPIO_INPUT);
	GPIOInit(TRIGGER,GPIO_OUTPUT);
	  return (true);
}
//esta función lee la distancia en centimetros, para eso se fija cuando el echo esta en alto y cuanta los microsegundos que esta en ese estado (en alto). Luego con una fórmula lo convierte en distancia en cm
int16_t HcSr04ReadDistanceCentimeters(void){
	bool State_Pin= GPIORead(ECHO);//me devuelve un true si hay ECHO (entrada)
	int16_t Cont_usec=0;
	GPIOOn(TRIGGER);//prendo el trigger
	DelayUs(10);//lo retraso 10 microsegundos porque eso dice el problema
	GPIOOff(TRIGGER);//apago el trigger
	while(State_Pin==0)
	{
		State_Pin= GPIORead(ECHO);
	}
	while(State_Pin==true)
	{
		DelayUs(1);//me asegura que cuento cada 1 microsegundo
		State_Pin= GPIORead(ECHO);
		Cont_usec++;
	}
	Distance_Centimeters=Cont_usec*(340*100)/1000000;//conversión a distancia en cm
	return (Distance_Centimeters);
}

int16_t HcSr04ReadDistanceInches(void){//esta funcion lee la distancia en pulgadas
	Distance_Inches=HcSr04ReadDistanceCentimeters()*2.54;
	return(Distance_Inches);
}

bool HcSr04Deinit(gpio_t echo, gpio_t trigger){//esta función es para liberar memoria
	ECHO=echo;
	TRIGGER=trigger;
	GPIODeinit();
	return (true);
}



