/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

typedef struct{
	uint8_t port;
	uint8_t pin;
	uint8_t dir;
}GpioConf_t;

void ManejarGpio(GpioConf_t *vector_bits, uint8_t bcd){
	uint8_t i, mascara;
	printf(" %d",bcd);
	mascara=0;
	for(i=0; i<4; i++){
		if((bcd&(mascara|(1<<i)))!=0){
			printf("\r\n El valor de salida del puerto %d", vector_bits[3-i].port);
			printf(".%d", vector_bits[3-i].pin);
			printf(" es: %d", 1);
		}
		else{
			printf("\r\n El valor de salida del puerto %d", vector_bits[3-i].port);
			printf(".%d", vector_bits[3-i].pin);
			printf(" es: %d", 0);
		}
	}


}


int main(void)
{
	GpioConf_t vector_puertos[4];
	vector_puertos[0].port=1;
	vector_puertos[0].pin=4;
	vector_puertos[0].dir=1;
	vector_puertos[1].port=1;
	vector_puertos[1].pin=5;
	vector_puertos[1].dir=1;
	vector_puertos[2].port=1;
	vector_puertos[2].pin=6;
	vector_puertos[2].dir=1;
	vector_puertos[3].port=2;
	vector_puertos[3].pin=14;
	vector_puertos[3].dir=1;
	uint8_t mibcd;
	mibcd=0b1001;
	ManejarGpio(vector_puertos, mibcd);

	return 0;
}

/*==================[end of file]============================================*/

