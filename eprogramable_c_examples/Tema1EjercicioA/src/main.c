/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/*Declare una variable sin signo de 32 bits y cargue el valor 0x01020304. Declare cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento, cargue cada uno de los bytes de la variable de 32 bits.
Realice el mismo ejercicio, utilizando la definición de una “union”.
 */
#define ON 1
#define OFF 0
#define TOGGLE 2
typedef struct
{
	uint8_t n_led;     // indica el número de led a controlar
	uint8_t n_ciclos;  // indica la cantidad de cilcos de encendido/apagado
	uint8_t periodo;   // indica el tiempo de cada ciclo
	uint8_t mode;      // ON, OFF, TOGGLE
}leds;

void Parpadear(uint8_t period, uint8_t num_led){
	printf("\r\n Encender led %d", num_led);
	uint8_t i;
	for(i=0; i<period; i++){

	}
	printf("\r\n Apagar led %d", num_led);
}

void OperarLed(leds *x){
	uint8_t i=0;
	switch (x->mode){
		case ON:
			switch (x->n_led){
				case 1:
					printf("\r\n Encender led 1");
					break;
				case 2:
					printf("\r\n Encender led 2");
					break;
				case 3:
					printf("\r\n Encender led 3");
					break;
				default:
					printf("\r\n Numero de led invalido");
					break;
				}
			break;
		case OFF:
			switch (x->n_led){
				case 1:
					printf("\r\n Apagar led 1");
					break;
				case 2:
					printf("\r\n Apagar led 2");
					break;
				case 3:
					printf("\r\n Apagar led 3");
					break;
				default:
					printf("\r\n Numero de led invalido");
					break;
				}
			break;
		case TOGGLE:
			printf("\r\n Parpadear led %d", x->n_led);
			switch (x->n_led){
				case 1:
					for(i=0; i<x->n_ciclos; i++){
						Parpadear(x->periodo, x->n_led);
					}
					break;
				case 2:
					for(i=0; i<x->n_ciclos; i++){
						Parpadear(x->periodo, x->n_led);
					}
					break;
				case 3:
					for(i=0; i<x->n_ciclos; i++){
						Parpadear(x->periodo, x->n_led);
					}
					break;
				default:
					printf("\r\n Numero de led invalido");
					break;
				}
			break;
		default:
			printf("\r\n El modo ingresado es invalido");
			break;

	}
}


int main(void){

/*
	leds ledx;
	ledx.mode=1;
	ledx.n_ciclos=5;
	ledx.n_led=3;
	ledx.periodo=50;
	OperarLed(&ledx);
*/

	return 0;
}

/*==================[end of file]============================================*/

