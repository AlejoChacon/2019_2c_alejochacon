Universidad Nacional de Entre Rios - Facultad de Ingeniería

Cátedra: Electrónica Programable - 2° cuatrimestre 2019

Recuperatorio parcial individual

Autor: Chacon, Alejo


Periféricos: 

*Display LCD ITS-E0803
*Acelerómetro de salida analógica (modelo desconocido)
*Módulo de transmisión bluetooth HC-05 (baudrate de la comunicación serie especificada para este modelo)


Bibliotecas utilizadas

*analog_io.h
*systemclock.h
*timer.h
*gpio.h
*uart.h
*DisplayITS_E0803.h
*switch.h
*bool.h


Introduccion

La aplicación está desarrollada en lenguaje C para su uso con la placa EDU-CIAA. La misma recibe un dato analógico 
de un acelerómetro y lo convierte a digital para luego aplicar un filtrado provisto por la cátedra. Es posible 
modificar el parámetro lpf_beta del filtrado utilizando las teclas 2, 3 y 4 de la EDU-CIAA para que el mismo tome 
los valores 0.25, 0.55 y 0.75 respectivamente. Este valor es visualizado en el display LCD ITS-E0803 en todo momento.
Los valores en crudo y filtrado del acelerómetro son enviados a traves del módulo HC-05 mediante el protocolo RS-232.


Montaje e información importante

La salida analógica del acelerómetro debe ser conectada al puerto al canal CH1 de la EDU-CIAA.
Los pines del display deben ser conectados respetando la siguiente configuración:
	Pin0: LCD1
	Pin1: LCD2
	Pin2: LCD3
	Pin3: LCD4
	Pin4: GPIO1
	Pin5: GPIO3
	Pin6: GPIO5
En caso de utilizar un módulo bluetooth distinto al HC-05 se debe actualizar el baud rate del inicializador de la UART
para el nuevo módulo.