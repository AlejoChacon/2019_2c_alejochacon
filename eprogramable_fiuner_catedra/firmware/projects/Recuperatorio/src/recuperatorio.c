/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/recuperatorio.h"       /* <= own header */
#include "analog_io.h"
#include "systemclock.h"
#include "timer.h"
#include "gpio.h"
#include "uart.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "bool.h"


/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
bool ControlPrimerDato=false;
unit16_t DatoCrudoPrevio=0;
unit16_t DatoCrudoActual=0;
uint16_t ValorFiltradoActual=0;
unit16_t ValorFiltradoPrevio=0;
float16_t lpf_beta=0.55;

/*==================[internal functions declaration]=========================*/

/*
 * @fn Leer
 * @brier Lectura del dato analógico recibido del canal CH1
 * return void
 */
void Leer(){
	if(ControlPrimerDato==false){
		AnalogInputRead(CH1, &DatoCrudoPrevio);
		ControlPrimerDato=true;
	}
	else{
		DatoCrudoPrevio=DatoCrudoActual;
		AnalogInputRead(CH1, &DatoCrudoActual);
	}
}

/*
 * @fn Muestrear
 * @brief Inicia la conversión AD
 * @return void
 */
void Muestrear(){
	AnalogStartConvertion();
}

/*
 * @fn Filtrar
 * @brief Aplica un filtrado al valor analógico obtenido del acelerómetro
 * @return void
 */
void Filtrar(){
	ValorFiltradoActual=ValorFiltradoPrevio-(lpf_beta*(ValorFiltradoPrevio-DatoCrudoActual));
}

/*
 * @fn Tec2
 * @brief Modifica el valor lpf_beta a 0.25
 * @return void
 */
void Tec2(){
	lpf_beta=0.25;
}

/*
 * @fn Tec3
 * @brief Modifica el valor lpf_beta a 0.55
 * @return void
 */
void Tec3(){
	lpf_beta=0.55;
}

/*
 * @fn Tec4
 * @brief Modifica el valor lpf_beta a 0.75
 * @return void
 */
void Tec4(){
	lpf_beta=0.75;
}

/*
 * @fn EnviarDatos
 * @brief Envia por protocolo RS-232 el valor recibido bruto del acelerómetro y el mismo valor filtrado
 * @return void
 */
void EnviarDatos(){

	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(DatoCrudoActual, 10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR,", ");
	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(ValorFiltradoActual, 10));
	UartSendString(SERIAL_PORT_P2_CONNECTOR,"\r\n");
}

/*
 * @fn Useless()
 * @brief Funcion vacia para parametro *pSerial del inicializador de la UART
 * @return void
 */
void Useless(){

}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	analog_input_config ADC_Inicializar;
	ADC_Inicializar.input=CH1;
	ADC_Inicializar.mode=AINPUTS_SINGLE_READ;
	ADC_Inicializar.pAnalogInput=Leer;
   
	timer_config Inicializar_Timer;
	Inicializar_Timer.timer=TIMER_B;
	Inicializar_Timer.period=100;
	Inicializar_Timer.pFunc=Muestrear;

	void (*Tecla2)()=Tec2;
	void (*Tecla3)()=Tec3;
	void (*Tecla4)()=Tec4;
	SwitchActivInt(SWITCH_2, Tecla2);
	SwitchActivInt(SWITCH_3, Tecla3);
	SwitchActivInt(SWITCH_4, Tecla4);

	gpio_t pins[7];
		pins[0]=LCD1;
		pins[1]=LCD2;
		pins[2]=LCD3;
		pins[3]=LCD4;
		pins[4]=GPIO1;
		pins[5]=GPIO3;
		pins[6]=GPIO5;

	serial_config UARTInitializer;
	UARTInitializer.port=SERIAL_PORT_P2_CONNECTOR;
	UARTInitializer.baud_rate=9600;
	UARTInitializer.pSerial=Useless;


	AnalogInputInit(&ADC_Inicializar);
	TimerInit(&Inicializar_Timer);
	SwitchesInit();
	ITSE0803Init(pins);
	UartInit(&UARTInitializer);

	TimerStart(TIMER_B);
    while(1){
    	Filtrar();
    	ITSE0803DisplayValue(lpf_beta*100);
    	EnviarDatos();
	}
    
	return 0;
}

/*==================[end of file]============================================*/

