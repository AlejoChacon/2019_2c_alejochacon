/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * Benitez Cerquetella, Virginia - vir_be@hotmail.com
 * Chacon, Alejo - alejochacon96@gmail.com
 * Ratti, Tomas - tomasratti@gmail.com
 * Romero Ravano,  Maria Victoria - victoriaremo@gmail.com
 *

 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/IR_Test.h"       /* <= own header */

#include "Tcrt5000.h"
#include "led.h"
#include "systemclock.h"
#include "bool.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "delay.h"

/* Aplicacion C para utilizar sensor infrarrojo Tcrt5000 como contador de obstaculos
 * Led rojo de EDU-CIAA indica que no se sensa un obstaculo, led verde indica un obstaculo
 * El valor obtenido se presenta en display ITS_E0803
 * La cuenta se incrementa cuando el estado cambia de obstaculo a no obstaculo (verde a rojo)
 * Presionando tecla 1 de EDU-CIAA se reinicia la cuenta .
 */

bool Hold=false;
bool OnOff=false;
uint8_t LineCount=0;

void Tec1(){
	if(OnOff==false){
		OnOff=true;
		LedOn(LED_RGB_B);
	}
	else{
		OnOff=false;
		LedOff(LED_RGB_B);
	}
}

void Tec2(){
	if(Hold==false){
		Hold=true;
		LedOn(LED1);
	}
	else{
		Hold=false;
		LedOff(LED1);
	}
}

void Tec3(){
	if(Hold==false){
		LineCount=0;
	}
	LedOn(LED_2);
	DelayMs(250);
	LedOff(LED_2);
}


int main(void)
{
	gpio_t x;
 	x=T_COL0;
	bool PreviousState, ActualState;

	SystemClockInit();
	Tcrt5000Init(x);
	LedsInit();
	PreviousState=ActualState=Tcrt5000State();

	void (*Tecla1)()=Tec1;
	void (*Tecla2)()=Tec2;
	void (*Tecla3)()=Tec3;

	SwitchActivInt(SWITCH_1, Tecla1);
	SwitchActivInt(SWITCH_2, Tecla2);
	SwitchActivInt(SWITCH_3, Tecla3);

	gpio_t pins[7];
		pins[0]=LCD1;
		pins[1]=LCD2;
		pins[2]=LCD3;
		pins[3]=LCD4;
		pins[4]=GPIO1;
		pins[5]=GPIO3;
		pins[6]=GPIO5;
	ITSE0803Init(pins);

	SwitchesInit();

	while(1){
		if(OnOff==true){
			ITSE0803OFF();
			LineCount=0;
		}

		else{
			if(Hold==false){
				ActualState=Tcrt5000State();
				if(ActualState==0){
					if(ActualState!=PreviousState){
					LineCount++;}
				}

			PreviousState=ActualState;
			ITSE0803DisplayValue(LineCount);
			}
		}
	}

return 0;
}

/*==================[end of file]============================================*/

