Universidad Nacional de Entre Rios - Facutad de Ingeniería

Proyecto 4: "Detector de frecuencias con indicador luminico"

Catedra: Electronica Programable - 2° cuatrimestre 2019

Autores: Benitez Cerquetella, Virginia
	 Chacon, Alejo

Colaboraciones: Filomena, Eduardo
	 	Furios, Joaquin
		Mateos, Sebastian


Periféricos

Anillo NeoPixel de 12 LEDs (Vcc=5V)
Módulo de transmisión y recepción bluetooth HC-05 (Vcc=5V)
Micrófono de salida analógica (Vcc=3.3V)


Bibliotecas utilizadas

systemclock.h
analog_io.h
timer.h (adaptada para tener un timer de 10us TIMERC)
gpio.h
uart.h
bool.h
NeoPixel.h
fpu_init.h
amr_math.h *
arm_const_structs.h *
ws2812b.h **

*Para usar estas bibliotecas fue necesario modificar las bibliotecas de ARM (Sebastian Mateos), utilizar el repositorio presentado para evitar problemas de funcionamiento
**Esta biblioteca se encuentra incluida dentro del archivo NeoPixel.c y es necesaria para su correcto funcionamiento


Introduccion

El software está escrito en el lenguaje de alto nivel C# y esta preparado para funcionar en la placa EDU-CIAA provista por la cátedra, el mismo se encarga de analizar una 
señal de audio analógica (frecuencia de muestreo de 20kHz y ventana de análisis de 2048 elementos) y basandose en la frecuencia de mayor intensidad de la misma detectará su 
nota principal y encendera uno de los 12 diodos LED del anillo NeoPixel, dependiendo la ubicación del led se puede discriminar entre las 7 notas principales 
(DO-RE-MI-FA-SOL-LA-SI) y sus 5 sostenidos (DO#-RE#-FA#-SOL#-LA#), y dependiendo el color (rojo-naranja-amarillo-verde-celeste-azul-violeta) se puede determinar a cual de las 
7 octavas principales pertenece la nota. Debido a limitaciones de la máxima frecuencia de muestreo y a la sensibilidad del micrófono utilizado es necesario realizar el montaje
en un lugar con la menor cantidad de ruido posible para evitar errores en la deteccion de bajas frecuencias (menores a 250 Hz), de otro modo las primeras 3 octavas pueden no 
ser detectadas correctamente. El software permite, además, recibir la nota y la octava en un receptor bluetooth por medio del protocolo serie RS-232.


Montaje e información importante

La salida analógica del micrófono debe ir al canal analógico CH1 de la EDU-CIAA
La frecuencia de lectura esta programada para el módulo HC-05 en 9600 Baudios, en caso de no disponer del mismo módulo modificarla segun los requerimientos del utilizado
La entrada de datos para el anillo NeoPixel se realiza a traves del puerto GPIO1
Debido a que para frecuencias de muestreo mayores a los 20kHz o para anchos de ventana menores a 2048 el cálculo de la transformada de Fourier (y de las frecuencias de la 
señal) se volvía sub-óptimo, la máxima resolución frecuencial quedó establecida en los 9.77Hz por lo que las primeras dos octavas estan muy limitadas y la tercera saltea 
algunas notas.

