/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto4.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "analog_io.h"
#include "timer.h"
#include "gpio.h"
#include "uart.h"
#include "bool.h"
#include "NeoPixel.h"
#include "fpu_init.h"

#include "arm_math.h"
#include "arm_const_structs.h"


/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000
#define ARM_MATH_CM4
#define __FPU_PRESENT 1
#define IFFT_FLAG 0
#define DO_BIT_REVERSE 1
#define WINDOW_WIDTH 2048
#define Fm 20000

#define DO		1
#define DO_S	2
#define RE		3
#define RE_S	4
#define MI		5
#define FA		6
#define FA_S	7
#define SOL		8
#define SOL_S	9
#define LA		10
#define LA_S	11
#define SI		12

/*==================[internal data definition]===============================*/

uint8_t octava;
uint8_t nota;
float32_t frecuencia;
float32_t VectorAD[2*WINDOW_WIDTH];
uint16_t Analog_single_data;
uint16_t Contador=0;
bool new_window = false;


/*==================[internal functions declaration]=========================*/

/*
 * @fn ContadorDeMuestras
 * @brief Inicia la conversión AD
 * @return null
 */
void ContadorDeMuestras(){

	if(Contador<WINDOW_WIDTH){
		AnalogStartConvertion();
	}
	else{
		TimerStop(TIMER_C);
		Contador=0;
		new_window = true;
	}
}

/*
 * @fn Leer
 * @brief Lee la señal analógica del canal 1 y la almacena en VectorAD
 * @return null
 */
void Leer(){
	AnalogInputRead(CH1, &Analog_single_data);
	VectorAD[Contador*2]=Analog_single_data*(3.3/1024);
	VectorAD[Contador*2+1]=0;
	Contador++;
}

/*
 * @fn DetOctavaYNota
 * @brief Calcula la frecuencia basandose en la posicion de mayor intensidad del vector fft y determina a que nota y octava corresponde
 * @return null
 */
void DetOctavaYNota(uint16_t posicion){

	frecuencia=(Fm/(2*WINDOW_WIDTH))*(posicion-0.5);
	if(frecuencia<64){
		octava=1;
		if(frecuencia<33.5){
			nota=DO;
		}
		else if(frecuencia<35.5){
			nota=DO_S;
		}
		else if(frecuencia<37.5){
			nota=RE;
		}
		else if(frecuencia<40){
			nota=RE_S;
		}
		else if(frecuencia<42.5){
			nota=MI;
		}
		else if(frecuencia<45){
			nota=FA;
		}
		else if(frecuencia<47.5){
			nota=FA_S;
		}
		else if(frecuencia<50.5){
			nota=SOL;
		}
		else if(frecuencia<53.5){
			nota=SOL_S;
		}
		else if(frecuencia<57){
			nota=LA;
		}
		else if(frecuencia<60){
			nota=LA_S;
		}
		else if(frecuencia<64){
			nota=SI;
		}
	}

	else if(frecuencia<128){
		octava=2;
		if(frecuencia<67){
			nota=DO;
		}
		else if(frecuencia<71){
			nota=DO_S;
		}
		else if(frecuencia<75){
			nota=RE;
		}
		else if(frecuencia<80){
			nota=RE_S;
		}
		else if(frecuencia<85){
			nota=MI;
		}
		else if(frecuencia<90){
			nota=FA;
		}
		else if(frecuencia<95){
			nota=FA_S;
		}
		else if(frecuencia<101){
			nota=SOL;
		}
		else if(frecuencia<107){
			nota=SOL_S;
		}
		else if(frecuencia<114){
			nota=LA;
		}
		else if(frecuencia<120){
			nota=LA_S;
		}
		else if(frecuencia<128){
			nota=SI;
		}
	}

	else if(frecuencia<256){
		octava=3;
		if(frecuencia<134){
			nota=DO;
		}
		else if(frecuencia<142){
			nota=DO_S;
		}
		else if(frecuencia<150){
			nota=RE;
		}
		else if(frecuencia<160){
			nota=RE_S;
		}
		else if(frecuencia<170){
			nota=MI;
		}
		else if(frecuencia<180){
			nota=FA;
		}
		else if(frecuencia<190){
			nota=FA_S;
		}
		else if(frecuencia<202){
			nota=SOL;
		}
		else if(frecuencia<214){
			nota=SOL_S;
		}
		else if(frecuencia<228){
			nota=LA;
		}
		else if(frecuencia<240){
			nota=LA_S;
		}
		else if(frecuencia<256){
			nota=SI;
		}
	}

	else if(frecuencia<512){
		octava=4;
		if(frecuencia<268){
			nota=DO;
		}
		else if(frecuencia<284){
			nota=DO_S;
		}
		else if(frecuencia<300){
			nota=RE;
		}
		else if(frecuencia<320){
			nota=RE_S;
		}
		else if(frecuencia<340){
			nota=MI;
		}
		else if(frecuencia<360){
			nota=FA;
		}
		else if(frecuencia<380){
			nota=FA_S;
		}
		else if(frecuencia<404){
			nota=SOL;
		}
		else if(frecuencia<428){
			nota=SOL_S;
		}
		else if(frecuencia<456){
			nota=LA;
		}
		else if(frecuencia<480){
			nota=LA_S;
		}
		else if(frecuencia<512){
			nota=SI;
		}
	}

	else if(frecuencia<1024){
		octava=5;
		if(frecuencia<536){
			nota=DO;
		}
		else if(frecuencia<568){
			nota=DO_S;
		}
		else if(frecuencia<600){
			nota=RE;
		}
		else if(frecuencia<640){
			nota=RE_S;
		}
		else if(frecuencia<680){
			nota=MI;
		}
		else if(frecuencia<720){
			nota=FA;
		}
		else if(frecuencia<760){
			nota=FA_S;
		}
		else if(frecuencia<808){
			nota=SOL;
		}
		else if(frecuencia<856){
			nota=SOL_S;
		}
		else if(frecuencia<912){
			nota=LA;
		}
		else if(frecuencia<960){
			nota=LA_S;
		}
		else if(frecuencia<1024){
			nota=SI;
		}
	}

	else if(frecuencia<2048){
		octava=6;
		if(frecuencia<1072){
			nota=DO;
		}
		else if(frecuencia<1136){
			nota=DO_S;
		}
		else if(frecuencia<1200){
			nota=RE;
		}
		else if(frecuencia<1280){
			nota=RE_S;
		}
		else if(frecuencia<1360){
			nota=MI;
		}
		else if(frecuencia<1440){
			nota=FA;
		}
		else if(frecuencia<1520){
			nota=FA_S;
		}
		else if(frecuencia<1616){
			nota=SOL;
		}
		else if(frecuencia<1712){
			nota=SOL_S;
		}
		else if(frecuencia<1824){
			nota=LA;
		}
		else if(frecuencia<1920){
			nota=LA_S;
		}
		else if(frecuencia<2048){
			nota=SI;
		}
	}

	else if(frecuencia<4096){
		octava=7;
		if(frecuencia<2144){
			nota=DO;
		}
		else if(frecuencia<2272){
			nota=DO_S;
		}
		else if(frecuencia<2400){
			nota=RE;
		}
		else if(frecuencia<2560){
			nota=RE_S;
		}
		else if(frecuencia<2720){
			nota=MI;
		}
		else if(frecuencia<2880){
			nota=FA;
		}
		else if(frecuencia<3040){
			nota=FA_S;
		}
		else if(frecuencia<3232){
			nota=SOL;
		}
		else if(frecuencia<3424){
			nota=SOL_S;
		}
		else if(frecuencia<3648){
			nota=LA;
		}
		else if(frecuencia<3840){
			nota=LA_S;
		}
		else if(frecuencia<4096){
			nota=SI;
		}
	}

}

/*
 * @fn Afinador
 * @brief Enciende un led con un color determinado basandose en la nota y la octava
 * @return null
 */
void Afinador(){
	NeoPixelAllOFF();
	switch(octava){
		case 1:		/*Octava 1 --> Color Rojo */
			NeoPixelSingleLed(0,255,0,nota,1);
			break;
		case 2:		/*Octava 2 --> Color Naranja */
			NeoPixelSingleLed(100,255,0,nota,1);
			break;
		case 3:		/*Octava 3 --> Color Amarillo */
			NeoPixelSingleLed(255,255,0,nota,1);
			break;
		case 4:		/*Octava 4 --> Color Verde */
			NeoPixelSingleLed(255,0,0,nota,1);
			break;
		case 5:		/*Octava 5 --> Color Celeste */
			NeoPixelSingleLed(255,0,255,nota,1);
			break;
		case 6:		/*Octava 6 --> Color Azul */
			NeoPixelSingleLed(0,0,255,nota,1);
			break;
		case 7:		/*Octava 7 --> Color Violeta */
			NeoPixelSingleLed(0,100,255,nota,1);
			break;
		default:
			break;
	}
}

/*
 * @fn Useless
 * @brief Funcion vacia para inicializar la interrupcion de la UART
 */
void Useless(){

}

/*
 * @fn EnviarInfo
 * @brief Envia la nota y la octava por protocolo RS232 a un receptor bluetooth
 * @return null
 */

void EnviarInfo(){

	switch(nota){
		case 1:
			UartSendString(SERIAL_PORT_P2_CONNECTOR,"\r\n DO");
			break;
		case 2:
			UartSendString(SERIAL_PORT_P2_CONNECTOR,"\r\n DO#");
			break;
		case 3:
			UartSendString(SERIAL_PORT_P2_CONNECTOR,"\r\n RE");
			break;
		case 4:
			UartSendString(SERIAL_PORT_P2_CONNECTOR,"\r\n RE#");
			break;
		case 5:
			UartSendString(SERIAL_PORT_P2_CONNECTOR,"\r\n MI");
			break;
		case 6:
			UartSendString(SERIAL_PORT_P2_CONNECTOR,"\r\n FA");
			break;
		case 7:
			UartSendString(SERIAL_PORT_P2_CONNECTOR,"\r\n FA#");
			break;
		case 8:
			UartSendString(SERIAL_PORT_P2_CONNECTOR,"\r\n SOL");
			break;
		case 9:
			UartSendString(SERIAL_PORT_P2_CONNECTOR,"\r\n SOL#");
			break;
		case 10:
			UartSendString(SERIAL_PORT_P2_CONNECTOR,"\r\n LA");
			break;
		case 11:
			UartSendString(SERIAL_PORT_P2_CONNECTOR,"\r\n LA#");
			break;
		case 12:
			UartSendString(SERIAL_PORT_P2_CONNECTOR,"\r\n SI");
			break;
		default:
			break;
	}
	UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(octava, 10));

}


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	uint16_t i, i_max;
	float32_t fft[WINDOW_WIDTH], max = 0;

	analog_input_config ADC_Inicializar;
	ADC_Inicializar.input=CH1;
	ADC_Inicializar.mode=AINPUTS_SINGLE_READ;
	ADC_Inicializar.pAnalogInput=Leer;


	timer_config Inicializar_Timer;
	Inicializar_Timer.timer=TIMER_C;
	Inicializar_Timer.period=100;
	Inicializar_Timer.pFunc=ContadorDeMuestras;


	serial_config UARTInitializer;
	UARTInitializer.port=SERIAL_PORT_P2_CONNECTOR;
	UARTInitializer.baud_rate=9600;
	UARTInitializer.pSerial=Useless;



	SystemClockInit();
	fpuInit();
	NeoPixelInit(1, 12);
	AnalogInputInit(&ADC_Inicializar);
	TimerInit(&Inicializar_Timer);
	UartInit(&UARTInitializer);

	TimerStart(TIMER_C);
    while(1){

    	if(new_window==true){
    		/* Procesamiento para realizar la FFT */
    		arm_cfft_f32(&arm_cfft_sR_f32_len512, VectorAD, IFFT_FLAG, DO_BIT_REVERSE);
    		arm_cmplx_mag_f32(VectorAD, fft, WINDOW_WIDTH);
    		new_window = false;

    		/* Obtencion del valor maximo del espectro de frecuencias */
    		for(i=2 ; i<WINDOW_WIDTH ; i++){
    			if(fft[i]>max){
    		 	max = fft[i];
    		  	i_max = i;
    		    	}
    			}
    		max = 0;
        	DetOctavaYNota(i_max);
        	Afinador();
        	EnviarInfo();
        	TimerStart(TIMER_C);
    	}


	}
    
	return 0;
}

/*==================[end of file]============================================*/

