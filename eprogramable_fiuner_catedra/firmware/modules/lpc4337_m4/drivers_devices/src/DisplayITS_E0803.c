/*
 * DisplayITS_E0803.c
 *
 *  Created on: 6/09/2019
 *  Author: Alejandro Fidalgo Badell
 */
/*==================[inclusions]=============================================*/
#include "DisplayITS_E0803.h"
#include "gpio.h"

/*==================[internal data definition]===============================*/

uint16_t valor_en_pantalla;

/*==================[internal functions definition]==========================*/
void DecimalToBcd(uint8_t digit){
	 switch(digit)
	     	{
	     		case 0:
	     			GPIOOff(LCD1);
	     			GPIOOff(LCD2);
	     			GPIOOff(LCD3);
	     			GPIOOff(LCD4);
	     		break;
	     		case 1:
	     			GPIOOn(LCD1);
	     			GPIOOff(LCD2);
	     			GPIOOff(LCD3);
	     			GPIOOff(LCD4);

	     		break;
	     		case 2:
	     			GPIOOff(LCD1);
	     			GPIOOn(LCD2);
	     			GPIOOff(LCD3);
	     			GPIOOff(LCD4);
	     		break;
	     		case 3:

	     			GPIOOn(LCD1);
	     			GPIOOn(LCD2);
	     			GPIOOff(LCD3);
	     			GPIOOff(LCD4);

	     		break;
	     		case 4:
	     			GPIOOff(LCD1);
	     			GPIOOff(LCD2);
	     			GPIOOn(LCD3);
	     			GPIOOff(LCD4);
	     		break;
	     		case 5:
	     			GPIOOn(LCD1);
	     			GPIOOff(LCD2);
	     			GPIOOn(LCD3);
	     			GPIOOff(LCD4);
	     		break;
	     		case 6:
	     			GPIOOff(LCD1);
	     			GPIOOn(LCD2);
	     			GPIOOn(LCD3);
	     			GPIOOff(LCD4);
	     		break;
	     		case 7:
	     			GPIOOn(LCD1);
	     			GPIOOn(LCD2);
	     			GPIOOn(LCD3);
	     			GPIOOff(LCD4);
	     		break;
	     		case 8:
	     			GPIOOff(LCD1);
	     			GPIOOff(LCD2);
	     			GPIOOff(LCD3);
	     			GPIOOn(LCD4);
	     		break;
	     		case 9:
	     			GPIOOn(LCD1);
	     			GPIOOff(LCD2);
	     			GPIOOff(LCD3);
	     			GPIOOn(LCD4);
	     		break;}
}
/*==================[external functions definition]==========================*/

bool ITSE0803Init(gpio_t *pins)
{
	/** Configuration of the GPIO */
	GPIOInit(pins[0], GPIO_OUTPUT);
	GPIOInit(pins[1], GPIO_OUTPUT);
	GPIOInit(pins[2], GPIO_OUTPUT);
	GPIOInit(pins[3], GPIO_OUTPUT);
	GPIOInit(pins[4], GPIO_OUTPUT);
	GPIOInit(pins[5], GPIO_OUTPUT);
	GPIOInit(pins[6], GPIO_OUTPUT);
	return true;
}
/*@brief function to set value*/
void ITSE0803DisplayValue(uint16_t valor)
{
	uint16_t aux,digito;
	aux = valor;
	valor_en_pantalla=valor;
	uint8_t j;
		 for(j=0;j<3;j++){
			GPIOOff(GPIO1);
		   	GPIOOff(GPIO3);
		   	GPIOOff(GPIO5);
			 digito= aux%10;
			 DecimalToBcd(digito);
			 switch(j)
			     	{
			     		case 0:
			     			GPIOOn(GPIO5);
			     		break;
			     		case 1:
			     			GPIOOn(GPIO3);
			     		break;
			     		case 2:
			     			GPIOOn(GPIO1);
			        	break;}
			 aux=aux/10;

		 }
}

void ITSE0803OFF(){
	uint8_t i;
	for(i=0; i<3; i++){
		GPIOOff(GPIO5);
		GPIOOff(GPIO3);
		GPIOOff(GPIO1);
		switch(i){
			case 0:
				GPIOOn(GPIO5);
				break;
			case 1:
				GPIOOn(GPIO3);
				break;
			case 2:
				GPIOOn(GPIO1);
				break;
		}
		GPIOOn(LCD1);
		GPIOOn(LCD2);
		GPIOOn(LCD3);
		GPIOOn(LCD4);
	}

}

uint16_t ITSE0803ReadValue(void){

	return(valor_en_pantalla);
}

bool ITSE0803Deinit(gpio_t *pins){

	GPIODeinit();
	return true;
}



/*==================[end of file]============================================*/
