/* Copyright 2016, XXXXXXXXXX
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef TIMER_H
#define TIMER_H
/** \brief Bare Metal example header file
 **
 ** This is a mini example of the CIAA Firmware
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */
/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup Baremetal Bare Metal example header file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 *
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * yyyymmdd v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/
#include "stdint.h"


/*==================[macros]=================================================*/
#define lpc4337            1
#define mk60fx512vlq15     2

#define TIMER_A 			0 				/*SysTick timer (CORTEX-M)*/
#define TIMER_B 			1 				/*RTI Timer (NXP)*/
#define TIMER_C				2

#define TIMER_A_10ms_TICK 	100
#define TIMER_B_10ms_TICK 	10
#define TIMER_C_10us_TICK	100000


/*==================[typedef]================================================*/
typedef struct {				/*!< Timer Struct*/
		uint8_t timer;			/*!< Timer Selected*/
		uint16_t period;		/*!< Period*/
		void *pFunc;			/*!< Function pointer to app repetitive function*/
} timer_config;
/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/
/*! \brief Función de inicialización del Cronometro.
 * Configura el cronómetro para trabajar con el SysTick y
 * vincula los eventos generados por el cronómetro a las funciones ejecutarse en la aplicación.
 * Se dispone de dos eventos: Ejecución periódica cada 100ms y Ejecución única al alcanzar un tiempo determinado
 *
 * \param[in] *punfc Puntero a función de ejecución periódica.
 * \param[in] *pAl Puntero a función de ejecución única (Alarma).
 */
//void InitCronometer(void *pfunc, void *pAl);
void TimerInit(timer_config *timer_ini);
void TimerStart(uint8_t timer);
void TimerStop(uint8_t timer);
void TimerReset(uint8_t timer);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef MI_NUEVO_PROYECTO_H */

/** @}*/
